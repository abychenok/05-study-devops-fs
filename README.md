# Домашнее задание к занятию "3.5. Файловые системы"

> 1. Узнайте о [sparse](https://ru.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B7%D1%80%D0%B5%D0%B6%D1%91%D0%BD%D0%BD%D1%8B%D0%B9_%D1%84%D0%B0%D0%B9%D0%BB) (разряженных) файлах.

Готово

> 2. Могут ли файлы, являющиеся жесткой ссылкой на один объект, иметь разные права доступа и владельца? Почему?

В Linux каждый файл имеет уникальный идентификатор - индексный дескриптор (inode). Это число, которое однозначно идентифицирует файл в файловой системе. Жесткая ссылка и файл, для которой она создавалась имеют одинаковые inode. Поэтому жесткая ссылка имеет те же права доступа, владельца и время последней модификации, что и целевой файл. Различаются только имена файлов. Фактически жесткая ссылка это еще одно имя для файла.

> 3. Сделайте `vagrant destroy` на имеющийся инстанс Ubuntu. Замените содержимое Vagrantfile следующим:

    ```bash
    Vagrant.configure("2") do |config|
      config.vm.box = "bento/ubuntu-20.04"
      config.vm.provider :virtualbox do |vb|
        lvm_experiments_disk0_path = "/tmp/lvm_experiments_disk0.vmdk"
        lvm_experiments_disk1_path = "/tmp/lvm_experiments_disk1.vmdk"
        vb.customize ['createmedium', '--filename', lvm_experiments_disk0_path, '--size', 2560]
        vb.customize ['createmedium', '--filename', lvm_experiments_disk1_path, '--size', 2560]
        vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', lvm_experiments_disk0_path]
        vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 2, '--device', 0, '--type', 'hdd', '--medium', lvm_experiments_disk1_path]
      end
    end
    ```

    Данная конфигурация создаст новую виртуальную машину с двумя дополнительными неразмеченными дисками по 2.5 Гб.

Пришлось переделать конфик ввиду того что я использую KVM. Вот мой Vagrantfile:

```bash
	Vagrant.configure("2") do |config|
	  config.vm.box = "generic/ubuntu2004"
	    config.vm.provider :libvirt do |v|
	    v.storage :file, :size => '2560MB', :bus => 'scsi', :type => 'raw'
	    v.storage :file, :size => '2560MB', :bus => 'scsi', :type => 'raw'
	  end
end
```

> 4. Используя `fdisk`, разбейте первый диск на 2 раздела: 2 Гб, оставшееся пространство.

fdisk /dev/sda
```
Command (m for help): g
Created a new GPT disklabel (GUID: 9F366EBC-AA6E-F842-A3EB-416A76FC7836).

Command (m for help): n
Partition number (1-128, default 1): 
First sector (2048-4999966, default 2048): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-4999966, default 4999966): 2G
Value out of range.
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-4999966, default 4999966): 2 G 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-4999966, default 4999966): +2G

Created a new partition 1 of type 'Linux filesystem' and of size 2 GiB.

Command (m for help): n
Partition number (2-128, default 2): 
First sector (4196352-4999966, default 4196352): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (4196352-4999966, default 4999966): 

Created a new partition 2 of type 'Linux filesystem' and of size 392.4 MiB.
w
```

> 5. Используя `sfdisk`, перенесите данную таблицу разделов на второй диск.

`sfdisk -d /dev/sda | sfdisk /dev/sdb`

> 6. Соберите `mdadm` RAID1 на паре разделов 2 Гб.

`mdadm --create --verbose /dev/md0 -l 1 -n 2 /dev/sda1 /dev/sdb1`

```
mdadm: Note: this array has metadata at the start and
    may not be suitable as a boot device.  If you plan to
    store '/boot' on this device please ensure that
    your boot-loader understands md/v1.x metadata, or use
    --metadata=0.90
mdadm: size set to 2094080K
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.
```

> 7. Соберите `mdadm` RAID0 на второй паре маленьких разделов.

`mdadm --create --verbose /dev/md1 -l 0 -n 2 /dev/sda2 /dev/sdb2`

```
mdadm: chunk size defaults to 512K
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md1 started.
```

> 8. Создайте 2 независимых PV на получившихся md-устройствах.

`pvcreate /dev/md0`
`pvcreate /dev/md1`

`pvdisplay`

```
  "/dev/md0" is a new physical volume of "<2.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/md0
  VG Name               
  PV Size               <2.00 GiB
  Allocatable           NO
  PE Size               0   
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               u4jfg2-8nzH-9OLz-LpE1-1ORX-Ilr2-eKC5OR
   
  "/dev/md1" is a new physical volume of "780.00 MiB"
  --- NEW Physical volume ---
  PV Name               /dev/md1
  VG Name               
  PV Size               780.00 MiB
  Allocatable           NO
  PE Size               0   
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               thVTNQ-IT5H-8KW6-6onm-5ql2-dhqw-cBuNkl
```

> 9. Создайте общую volume-group на этих двух PV.

`vgcreate vg1 /dev/md0 /dev/md1`

`vgdisplay`

```
--- Volume group ---
  VG Name               vg1
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               2.75 GiB
  PE Size               4.00 MiB
  Total PE              705
  Alloc PE / Size       0 / 0   
  Free  PE / Size       705 / 2.75 GiB
  VG UUID               apYo3i-Y9ty-s6UQ-hm8l-wx77-d4lC-kJX2ds
```

> 10. Создайте LV размером 100 Мб, указав его расположение на PV с RAID0.

`lvcreate -L 100MB -n data vg1 /dev/md1`

`lvdisplay`

```
  --- Logical volume ---
  LV Path                /dev/vg1/data
  LV Name                data
  VG Name                vg1
  LV UUID                A3fSnU-7Fsd-exJh-q29A-Mtoi-tsNZ-dWKUfT
  LV Write Access        read/write
  LV Creation host, time ubuntu2004.localdomain, 2022-03-28 18:10:29 +0000
  LV Status              available
  open                 0
  LV Size                100.00 MiB
  Current LE             25
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     4096
  Block device           253:0
```

> 11. Создайте `mkfs.ext4` ФС на получившемся LV.

`mkfs.ext4 /dev/vg1/data`

> 12. Смонтируйте этот раздел в любую директорию, например, `/tmp/new`.

`mkdir /tmp/new`
`mount -t ext4 /dev/vg1/data /tmp/new/`

> 13. Поместите туда тестовый файл, например `wget https://mirror.yandex.ru/ubuntu/ls-lR.gz -O /tmp/new/test.gz`.

Готово

> 14. Прикрепите вывод `lsblk`.

`lsblk`
```
NAME           MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda              8:0    0   2.4G  0 disk  
├─sda1           8:1    0     2G  0 part  
│ └─md0          9:0    0     2G  0 raid1 
└─sda2           8:2    0 392.4M  0 part  
  └─md1          9:1    0   780M  0 raid0 
    └─vg1-data 253:0    0   100M  0 lvm   /tmp/new
sdb              8:16   0   2.4G  0 disk  
├─sdb1           8:17   0     2G  0 part  
│ └─md0          9:0    0     2G  0 raid1 
└─sdb2           8:18   0 392.4M  0 part  
  └─md1          9:1    0   780M  0 raid0 
    └─vg1-data 253:0    0   100M  0 lvm   /tmp/new
vda            252:0    0   128G  0 disk  
├─vda1         252:1    0   487M  0 part  /boot
├─vda2         252:2    0   1.9G  0 part  [SWAP]
└─vda3         252:3    0 125.6G  0 part  /
```

> 15. Протестируйте целостность файла:
>
>    ```bash
>    root@vagrant:~# gzip -t /tmp/new/test.gz
>    root@vagrant:~# echo $?
>    0
>    ```
Готово

> 16. Используя pvmove, переместите содержимое PV с RAID0 на RAID1.

`pvmove -n data /dev/md1 /dev/md0`
```
  /dev/md1: Moved: 56.00%
  /dev/md1: Moved: 100.00%
```

> 17. Сделайте `--fail` на устройство в вашем RAID1 md.

`mdadm /dev/md0 --fail /dev/sda1`
```
mdadm: set /dev/sda1 faulty in /dev/md0
```

> 18. Подтвердите выводом `dmesg`, что RAID1 работает в деградированном состоянии.

```
[ 3468.070017] md/raid1:md0: Disk failure on sda1, disabling device.
               md/raid1:md0: Operation continuing on 1 devices.
```

> 19. Протестируйте целостность файла, несмотря на "сбойный" диск он должен продолжать быть доступен:
>
>    ```bash
>    root@vagrant:~# gzip -t /tmp/new/test.gz
>    root@vagrant:~# echo $?
>    0
>    ```
Готово

> 20. Погасите тестовый хост, `vagrant destroy`.
Готово